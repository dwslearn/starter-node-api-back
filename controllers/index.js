/* 
Definition
*/
    const Controllers = {
        auth: require('./auth.controller'),
        post: require('./post.controller'),
    }
//

/*  
Export
*/
    module.exports = Controllers;
//