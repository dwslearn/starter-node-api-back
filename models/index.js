/* 
Definition
*/
    const Models = {
        post: require('./post.model'),
        user: require('./user.model')
    } 
//

/* 
Export
*/
    module.exports = Models;
// 